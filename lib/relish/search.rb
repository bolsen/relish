module Relish::Search
  # This defines a single method for relations. Not to be used directly.

  private 

  def scan(relation = nil)

    # If relation == nil, then we do a full scan. 

    if relation.nil?
      body
    else
      if common.length == 0
        body
      else
        raise AssertionError, "This relation has 1 tuple" if body.length == 1 # ? what is this?

        rel_tupl = relation.scan().map {|t| t }
        rows = nil
        common.each do |attr|
          if rows.nil?
            rows = @heading_invert[attr][reltupl[attr]] || Set.new
          else
            rows = rows.intersection(@heading_invert[attr] || Set.new)
          end
        end

        rows
      end

    end
  end # if relation.nil?


end
