module Relish
  module Errors

    class InvalidOperationError < Exception; end
    class InvalidHeadingError < Exception; end
  end
end
