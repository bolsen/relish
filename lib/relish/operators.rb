require "relish/helpers"

module Relish::Operators

  # Finds the common attributes from current relation and provided relation.
  def common_attributes(relation)
    heading & relation.heading
  end

  def join(relation)
  end

  def and(relation)
    
  end

  def or(relation)

  end

  alias_method :"&&", :and
  alias_method :"||", :or
  alias_method :&, :join

  def intersection(relation)
    
  end

  def difference(relation)

  end

  alias_method :-, :difference

  def union(relation)

  end

  def rename(relation)

  end

  def group(relation)
    
  end

  def ungroup(relation)

  end

  def project(head, select = :just)
    raise RelationInvalidOperationError(self, "Heading attribute(s) should be an Array") if !heading.empty? && !heading.class == A
Array

    validate_heading(head)
    heading.each do |attr|
      raise RelationInvalidOperationError(self, "Heading attribute(s) should be an Array") if !(heading.any? {|a| a == attr })
      remove_heading = heading - head
    end

    remove(remove_heading)
  end


  def remove(head)
    validate_heading(head)
  end

  def restrict(hash_or_proc)

  end

  def extend(name, value)

  end
end
