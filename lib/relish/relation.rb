module Relish
  include Relish::Helpers

  def R(name, data)
    # We assume that the data for all the hashes in data are the same
    Relation.using(data[name])
  end

  # Example: query(:test => [{:a => 1}, {:a => 2}], :test2 => [{:a => 2}]) {|data| R(:test, data).join(R(:test2, data)) }
  # Result: Relation.new([:a], [{:a => 2}])

  def query(data, &block)
    block.call(data)
  end

  # A Relation is heading (using Strings or Symbols) and a body containing tuples that match
  # the heading of the relation.

  class Relation

    include Relish::Search

    attr_reader :heading, :body, :pk

    def initialize(heading, body = [], constraints = [])
      body = body.map do |t|
        if t.class == Hash
          Tuple.new(t)
        elsif t.class == Tuple
          t
        else
          raise InvalidOperationError, "Provided body does not contain Hash or Tuple."
        end
      end
      @heading = Set.new(heading)
      @body = body.class == Array ? Set.new(body) : body
      
      # @constraints = constraints.insert(constraints)
    end

    # Degree is the amount of tuples in the body.
    def degree
      body.length
    end

    # Cardinality is the amount of attributes in heading.
    def cardinality
      heading.length
    end
    
    def common(relation)
      heading.intersection(relation.heading)
    end


    # A join has multiple properties based the attributes it has. 
    # The definition of join is this: Assume two relations r and s.
    #
    # 1. r contains A1,A2,A3,Am...B1,B2,B3,Bn attributes.
    # 2. s contains B1,B2,B3,Bn...C1,C2,C3,Cp attributes.
    # 3. In other words, B1,B2,B3,Bn are the "common" attributes. We can assume that Am != Cp (no names are equal)
    # 4. r join s is the (set-theoretic) union of r's attributes and s's attributes (Am, Bn, Cp)
    # 5. r join s is the (set-theoretic) union of a tuple appearing in r (with an (Am,Bn) heading) and s (with a (Bn,Cp)) heading.
    # 6. IF m = p = 0, (there are no A's and C's) this means r and s have all attributes in common. If so, JOIN becomes INTERSECT.
    # 7. IF n = 0, (meaning there are no B's), this means r and s have no attributes in common. If this is so, JOIN becomes TIMES (cartesian product).
    #
    # Example:
    # author.rename(:id => :author_id).join(posts).group(:author_id => :post_group).sum(:post_group)
    #
    # The query joins authors with posts and the groups the posts by author. The resultant relations contains author information and how many posts they have.
    #
    # This can also be called AND.

    def join(relation)
      common = common(relation)
      if common.length > 0 && (common.length != @heading.length && common.length != relation.heading.length)
        # do natural join
        result = []
        body.each do |t1|
          tuples = (relation.body.select do |t2|
                      common.all? {|c| t1[c] == t2[c]}
                    end.map {|t2| [t1,t2]})
          if tuples.length > 0
            result << tuples
          end
        end

        rows= result.map do |tuple_arrays|
          rows = []
          tuple_arrays.each do |tuple|
            rows << tuple[0].merge(tuple[1])
          end
          
          rows
        end.flatten

        rows = rows.map {|r| Tuple.new(values = r) }


        Relation.new(heading + relation.heading, rows)

      elsif common.length == @heading.length && common.length == relation.heading.length
        # do intersection since all attributes are in common
        intersection(relation)
      elsif common.length == 0
        # do times (cartesian product) since no attribute is in common.
        times(relation)
      end
    end

    # alias_method :+, :join

    # An intersection is performed of two relations of the same type (the heading in both relations are the same.)
    # Assume two relations r and s, which happen to be of the same type. The intersection of r and s, then returns a relation where Tuples from both in r and s.

    def intersection(relation)
      if common(relation).length == @heading.length
        Relation.new(heading.intersection(relation.heading), body + relation.body.select {|b2| body.any? {|b1| b1.to_h === b2.to_h }})
      else
        raise InvalidOperationError, "Intersection not possible; headings do not match"
      end
    end


    def union(relation)
      if common(relation).length == @heading.length
        Relation.new(heading.union(relation.heading), body.to_a + relation.body.to_a)
      else
        raise InvalidOperationError, "Intersection not possible; headings do not match"
      end
    end


    def disjoint_union(relation)

    end

    # Relational times is the cartesian product between two relations. Assume two relations r and s, that contain no attributes in common. The cartesian prodct of these two relations the set theoretic union of the two relations heading, and the set-theoretic union of each tuple in r with a tuple in s.

    def times(relation)
      if common(relation).length == 0
        rows = []
        body.each do |tuple|

          if relation.body.length == 0
            rows << tuple
          else

            relation.body.each do |tuple2|
              rows << tuple.merge(tuple2)
            end

          end
        end

        Relation.new(heading.union(relation.heading), rows)
      else
        raise InvalidOperationError, ".times requires that both relations have no heading in common."
      end
    end

    
    # Matching, also called semijoin is a join between two relations, projected over the first relation.
    # Assume relations r and s. r.matching(s) is r.join(s), preserving only the attributes in r.
    def matching(relation)
      self.join(relation).project(*relation.heading.to_a)
    end

    
    # Projection returns a relation with only the attributes selected. Assume a relation r has a set of attributes Xn. If we do a projection like such: r.project(*X(n-1)), then the new relation will have a heading that is a subset of r's attributes, with a length one less than that of r's heading.
    def project(*head)
      check_heading_validity(head)
      check_attribute_validity(heading, head)

      new_head = heading - (heading - head)
      
      # TODO: projection can involve something simpler than running project on each
      # tuple
      Relation.new(new_head, body.map {|t| t.project(head) })
    end

    def remove(*head)
      check_heading_validity(head)
      check_attribute_validity(heading, head)
      new_head = heading - head

      Relation.new(new_head, body.map {|t| t.remove(*head) })
    end

    def rename(new_names)
      raise InvalidHeadingError, "Rename attributes must be in a Hash." unless new_names.class == Hash
      check_heading_validity(new_names.keys)
      check_attribute_validity(heading, new_names.keys)

      new_head = []
      new_names.each do |k,v|
        if heading.any? {|attr| attr == k }
          new_head << v
        end
      end

      new_head = heading - new_names.keys + new_head

      Relation.new(new_head, body.map {|t| t.rename(new_names) })

    end

    def where(&block)
      Relation.new(heading, body.select {|t| block.call(t) })
    end

    # Extend returns a new relation where it takes the attributes of an existing relation that is being
    # operated on, and supplies a new attribute that is the calculation from each tuple.
    #
    # This aids in writing calculated relations per tuple. Example:
    #
    # (person.extend(:how_old_in_ten_years) {|p| p.age + 10 }).remove(:age)

    def extend(attribute, &ext_block)
      raise InvalidHeadingError, "Heading attribute must be either Strings or Symbols." if attribute.class != String && attribute.class != Symbol
      
      new_head = heading + [attribute]
      Relation.new(new_head, body.map {|t| t.extend(attribute, &ext_block) })

    end

    # The common attributes of the given relations are removed after they are joined.
    # It comes from functional composition (Third Manifesto, 2nd Edition, p. 50)
    #
    # Example:
    #
    # r.compose(s)

    def compose(relation)
      remove(join(relation), common(relation))
    end


    # Summarize is a grouping/aggregation operation of sorts. 
    #
    # Assume two relations r and s, such that s.project(*attrs).heading == r.heading. Let the attributes
    # of s be A1, A2 ... An. So, a summarization like such:
    #
    # r.summarize(:X, :per => s) {|r, result| # summary function set to attribute X }
    #
    # would have a body with all tuples where t is a tuple of s extended with a value X.
    # X is computed by running the summary function over the body of r that have the same value 
    # for attributes A1,A2..An as the new tuples do. s cannot have an attribute called X. The cardinality
    # of s == the new relation's cardinality as well as the degree of s plus 1. 
    #
    # There are different summary types:
    # :per : A perfect example is provided by C.J. Date, in Databases In Depth:
    #
    # SP.summarize("P_COUNT", :per => S.project("SNO") {|SP, S, result| result = result + 1 }
    #
    # It scans the relation SP, finding where it matches with something in S.project("SNO") and counts the result up (like a COUNT() aggregate).
    #
    # Another way to look at that would be like this:
    #
    # SP.summarize("P_COUNT", :per => S.project("SNO") {|SP, St, result| SP.where {|sp| sp["SNO"] == St["SNO"] }.length }
    #
    # Summarizing without a qualified relation summarizes against the TableDee relation:
    #
    # (S.where {|s| s["CITY"] == "London"}).summarize("N") {|S, td, result| result = result + 1}
    #
    # More summarizations will be implemented in future versions:
    # 
    # :by : summarize against a project of the relation being operated on.
    #


    def summarize()
    end

    def group(group_name, head)
      
    end

    def ungroup(group_name)
    end

    def wrap(wrap_name, head)
      # The former attrs are just disappearing here...unwrap is not possible now mmm
      extend(wrap_name) do |t|
        t.project(*head)
      end.remove(*head)
    end

    def unwrap(wrap_name)

    end


    def generate(extension = {})
      extend(TableDee, extension.keys[0]) do |t|
        extension.values[0]
      end
    end

    def insert(relation)
    end

    def delete(relation)
    end

    def update(new_values, &block)
    end

  end


  # TableDee and TableDum (using Date's terminology) are two critical relation values in relational algebra.
  # TableDee is the identity relation with respect to join. so R JOIN TABLEDEE == R.
  TableDee = Relation.new([], [])
  TableDum = Relation.new([], [Tuple.new()])
 

end
