require "set"

module Relish

  # A Tuple is a hash-like object that has tuple operations. All tuple operations are <em>closed</em>, meaning
  # that each tuple operation returns a new Tuple. Hash-like operations can be performed on the tuples, since
  # it inherits the Hash class.
  #
  # 1. .project selects certain attributes
  # 2. .remove removes certain attributes
  # 3. .extend adds a new attribute based on the result of a computation.
  # 4. .wrap creates a new tuple of existing attributes and places the attribute under a new attribute name (a type of extend)
  # 5. .unwrap would do the opposite of .wrap.

  class Tuple < Hash
    
    include Relish::Helpers

    def initialize(values = {})
      values.each do |k,v|
        self[k] = v
      end
    end


    def attributes
      Set.new(keys)
    end

    def common(tuple)
      attributes.intersection(tuple.attributes)
    end

    # Tuple removal selects attributes to be removed, from which a new Tuple is created
    # with the attributes not removed remaining.
    #
    # Example: if we have a heading of [:a, :b, :c], and we don't want :c, Removal is:
    # [:a, :b, :c] - [:c] = [:a, :b]
    def remove(*head)
      check_heading_validity(head)
      check_attribute_validity(keys, head)
      new_head = keys - head
      Tuple.new(hash_from_keys(new_head))
    end


    # Tuple projection selects the attributes that will be part of a new Tuple.
    # Returns a new Tuple object.
    #
    # Example: if we have a heading of [:a, :b, :c], and we want just [:a, :b], :c will be
    # removed. Projection is:
    # [:a, :b, :c] - [:a, :b] = [:c] - [:a, :b, :c] = [:a, :b]
    
    def project(*head)
      check_heading_validity(head)
      check_attribute_validity(keys, head)
      
      new_head = keys - (keys - head)
      Tuple.new(hash_from_keys(new_head))
    end

    
    # Extends the tuple by a computable block. The block takes one attribute, which is the tuple itself.
    # Example: 
    #
    # <code>
    # tuple = Tuple.new(:a => 1, :b => 2, :c => 3)
    # tuple.extend(:calc_fields) {|t| t[:a] + t[:b] + t[:c] } 
    # => Tuple.new(:a => 1, :b => 2, :c => 3, :calc_fields => 6)
    
    def extend(attribute, &ext_block)
      raise InvalidHeadingError, "Heading attribute must be either Strings or Symbols." if attribute.class != String && attribute.class != Symbol
      Tuple.new(self.merge(attribute => ext_block.call(self)))
    end
    
    # Rename n number of attributes. Requires a hash of {OldName => NewName}.
    # Example:
    #
    # <code>
    # tuple = Tuple.new(:a => 1, :b => 2, :c => 3)
    # tuple.rename(:a => :t) => Tuple.new(:t => 1, :b => 2, :c => 3)
    # 
    def rename(new_names)
      raise InvalidHeadingError, "Rename attributes must be in a Hash." unless new_names.class == Hash
      check_heading_validity(new_names.keys)
      check_attribute_validity(keys, new_names.keys)

      nh = {}
      new_names.each do |k,v|
        nh[v] = self[k]
      end
     
      keys.each do |k|
        if new_names.keys.find {|a| a == k }.nil?
          nh[k] = self[k]
        end
      end

      Tuple.new(nh)
    end

    # Groups a number of attributes into a new tuple that is then stored in a new tuple under a particular attribute.
    # Example:
    # <code>
    # tuple = Tuple.new(:a => 1, :b => 2, :c => 3)
    # tuple.wrap(:b_and_c, [:b, :c])
    # => {:a => 1, :b_and_c => {:b => 2, :c => 3}}

    def wrap(wrap_name, head)
      extend(wrap_name) do |tuple|
        tuple.project(head)
      end.remove(head)
    end

    def unwrap(wrap_name)
      raise InvalidHeadingError, "Wrap name not found" if keys.find {|attr| attr == wrap_name}.nil?
      Tuple.new(self.merge(self[wrap_name])).remove([wrap_name])
    end

    begin
      require "ruport"
    rescue LoadError
      def to_record
        raise "Ruport could not be loaded."
      end

      def from_record
        raise "Ruport could not be loaded."
      end

    else
      def to_record
        Ruport::Data::Record.new(self)
      end

      def from_record(ruport_table)
      end

    end

    private

    def hash_from_keys(head)
      nh = {}
      head.each do |attr|
        nh[attr] = self[attr]
      end
      nh
    end


  end

end
