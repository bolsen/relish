module Relish::Helpers

  include Relish::Errors

  # Performs a number of validations on a heading if
  # 1. the heading is an Array
  # 2. if the length of the provided heading contains unique values
  # 3. If each heading attribute name is either a Symbol or a String only.
  #
  # If any of these validations fails, this method will throw a InvalidHeadingError.
  def check_heading_validity(heading)
    raise InvalidHeadingError, "Heading is not an Array." unless heading.class == Array
    raise InvalidHeadingError, "There are non-unique attributes in heading." if heading.length != heading.uniq.length
    heading.each do |attr|
      if attr.class != String && attr.class != Symbol
        raise InvalidHeadingError, "Heading attributes must be either Strings or Symbols." 
      end
    end
  end


  # Validates if the attributes in <em>new_head</em> are in the Array of attributes in <em>head</em>

  def check_attribute_validity(head, new_head)
    new_head.each do |attr|
      raise InvalidHeadingError, "Attribute #{attr} not in original heading." if head.find {|a| attr == a}.nil?
    end
  end


  def heading_and_values(heading, values)
    values.map {|value| heading.zip(value).flatten }.map {|t| Tuple.new(Hash.new(t)) }
  end

  alias_method :hv, :heading_and_values

end


module Relish
  class Relation
    begin
      require "ruport"
    rescue LoadError
      def to_table
        raise "Ruport could not be loaded."
      end

      def from_table
        raise "Ruport could not be loaded."
      end

    else
      def to_table
        Ruport::Data::Table.new(:column_names => heading.to_a, :data => body.to_a)
      end

      def from_table(ruport_table)
      end

      def to_s
        self.to_table.to_s
      end
    end
  end
end
