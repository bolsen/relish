require "test_helper"
require "relish/helpers"

include Relish::Helpers

class RelationTest < Test::Unit::TestCase
  

  describe "filling relations with data" do
    
    test "filling in data from an array" do
      rel = Relation.using([{:a => 1}, {:a => 2}, {:a => 3}, {:a => 4}])
      assert_equal 3, rel.degree
      assert_equal 1, rel.cardinality
    end

    test "filling in data from relation factory function" do
      data = {:test => [{:a => 1}, {:a => 2}, {:a => 3}]}
      rel = R(:test, data)
      assert_equal Relation.new([:a], hv(data[:test])), R(:test, data)
    end

    should "throw an error if the body has a non-Hash or non-Tuple." do
      assert_raises Relish::Errors::InvalidOperationError do
        # TODO: Maybe accept arrays in the future that follow the format to create
        # a new hash.
        Relation.new([:a], [{:a => 1}, [:a, 2]])
      end
      
    end

  end
  
  describe "unary relational operations" do
    
    should "project S to only have the S# attribute" do
      ps = S.project("S#")

      data = [["S1"], ["S2"], ["S3"], ["S4"], ["S5"]]

      assert_equal 1, ps.heading.length
      assert_equal Set.new(["S#"]), ps.heading
      assert_equal Set.new(heading_and_values(["S#"], data)), ps.body
      
    end

    should "project S on two attributes" do
      ps = S.project("S#", "SNAME")

      data = [["S1", "Smith"], ["S2", "Jones"], ["S3", "Blake"], ["S4", "Clark"], ["S5", "Adams"]]

      assert_equal 2, ps.heading.length
      assert_equal Set.new(["S#", "SNAME"]), ps.heading
      assert_equal Set.new(heading_and_values(["S#", "SNAME"], data)), ps.body
      
    end

    should "remove two attributes on S" do
      rs = S.remove("STATUS", "CITY")
      ps = S.project("S#", "SNAME")

      data = [["S1", "Smith"], ["S2", "Jones"], ["S3", "Blake"], ["S4", "Clark"], ["S5", "Adams"]]

      assert_equal ps, rs
      
      assert_equal 2, rs.heading.length
      assert_equal Set.new(["S#", "SNAME"]), rs.heading
      assert_equal Set.new(heading_and_values(["S#", "SNAME"], data)), rs.body
    end

    should "project to an attribute that contains duplicates" do
      # TODO: If body contains duplicate tuples before creating a relation after a projection,
      # then extend the relation with a summarization of how many values there are that were duplicated.
      # Example: if a relation will contain two tuples with 1 attribute called CITY with value of "Paris",
      # then add an attribute called "S_COUNT" that will have the number of duplicates there are.
      
      ps = S.project("CITY")
      rs = S.remove("S#", "SNAME", "STATUS")

      assert_equal ps, rs
      assert_equal 4, ps.body.length
      assert_equal 4, ps.body.length
      

    end


    should "contain an attribute that is a renaming of an existing attribute" do
      rename_s = S.rename("S#" => "S_NUM")
      
      assert_equal 4, rename_s.heading.length
      assert_equal Set.new(["S_NUM", "SNAME", "STATUS", "CITY"]), rename_s.heading
      rename_s.body.each do |t|
        assert_not_nil t["S_NUM"]
      end
      
    end


    should "restrict the S relation by CITY equaling 'London'" do
      s_london = S.where {|s| s["CITY"] == "London" }
      assert_equal 2, s_london.body.length
      
      s_london.body.each do |s|
        assert_equal "London", s["CITY"]
      end
    end
    
  end


  describe "simple dyadic operations" do

    # "common" is not a relational operation, but a set operation.
    test "that two relations have common attributes verifiably" do
      assert_equal Set.new("CITY"), S.common(SP)
      assert_equal Set.new("S#"), S.common(SP)
      assert_equal Set.new("P#"), P.common(SP)
      
      # Assert that they work opposite (communativity)
      assert_equal Set.new("CITY"), SP.common(S)
      assert_equal Set.new("S#"), SP.common(S)
      assert_equal Set.new("P#"), SP.common(S)
    end

    

  end

  describe "join operations" do
    test "a simple join that lists all the parts there are for supplier S1" do
      sp = S.where {|s| s["S#"] == "S1" }.join(SP)
      assert_equal 6, sp.body.length

      # sp == sp1, only that the search occurs on S.join(SP) instead of S first.
      sp1 = S.join(SP).where {|s| s["S#"] == "S1" }
      assert_equal sp, sp1
      
    end
   
    test "a join where it lists each part for suppler S4." do
      
      supplier_parts = S.where {|s| s["S#"] == "S4"}.join(SP).join(P).project { "S#", "P#" }
      # can also be expressed as:
      # S.where {|s| s["S#"] == "S1"}.join(SP, P).project {"S#", "P#"}
      # S.join(SP, P).project {"S#", "P#"}.where {|s| s["S#"] == "S1" }

      # supplier_parts should have 4 tuples, each with a cardinality of 2 ["S#", "P#"].

      assert_equal 2, supplier_parts.degree
      

    end

  end

end
