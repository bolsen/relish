require "relish"

include Relish

S = Relation.new(["S#", "SNAME", "STATUS", "CITY"], 
                 [Tuple.new("S#" => "S1", "SNAME" => "Smith", "STATUS" => 20, "CITY" => "London"),
                  Tuple.new("S#" => "S2", "SNAME" => "Jones", "STATUS" => 10, "CITY" => "Paris"),
                  Tuple.new("S#" => "S3", "SNAME" => "Blake", "STATUS" => 30, "CITY" => "Paris"),
                  Tuple.new("S#" => "S4", "SNAME" => "Clark", "STATUS" => 20, "CITY" => "London"),
                  Tuple.new("S#" => "S5", "SNAME" => "Adams", "STATUS" => 30, "CITY" => "Athens")])

P = Relation.new(["P#", "PNAME", "COLOR", "WEIGHT", "CITY"],
                 [Tuple.new("P#" => "P1", "PNAME" => "Nut", "COLOR" => "Red", "WEIGHT" => 12, "CITY" => "London"),
                  Tuple.new("P#" => "P2", "PNAME" => "Bolt", "COLOR" => "Green", "WEIGHT" => 17, "CITY" => "Paris"),
                  Tuple.new("P#" => "P3", "PNAME" => "Screw", "COLOR" => "Blue", "WEIGHT" => 17, "CITY" => "Rome"),
                  Tuple.new("P#" => "P4", "PNAME" => "Screw", "COLOR" => "Red", "WEIGHT" => 14, "CITY" => "London"),
                  Tuple.new("P#" => "P5", "PNAME" => "Cam", "COLOR" => "Blue", "WEIGHT" => 12, "CITY" => "Paris"),
                  Tuple.new("P#" => "P6", "PNAME" => "Cog", "COLOR" => "Red", "WEIGHT" => 19, "CITY" => "London")])

SP = Relation.new(["S#", "P#", "QTY"],
                  [Tuple.new("S#" => "S1", "P#" => "P1", "QTY" => 300),
                   Tuple.new("S#" => "S1", "P#" => "P2", "QTY" => 300),
                   Tuple.new("S#" => "S1", "P#" => "P3", "QTY" => 300),
                   Tuple.new("S#" => "S1", "P#" => "P4", "QTY" => 300),
                   Tuple.new("S#" => "S1", "P#" => "P5", "QTY" => 300),
                   Tuple.new("S#" => "S1", "P#" => "P6", "QTY" => 300),
                   Tuple.new("S#" => "S2", "P#" => "P1", "QTY" => 300),
                   Tuple.new("S#" => "S3", "P#" => "P2", "QTY" => 300),
                   Tuple.new("S#" => "S4", "P#" => "P2", "QTY" => 300),
                   Tuple.new("S#" => "S4", "P#" => "P2", "QTY" => 300),
                   Tuple.new("S#" => "S4", "P#" => "P4", "QTY" => 300),
                   Tuple.new("S#" => "S4", "P#" => "P5", "QTY" => 300)])
